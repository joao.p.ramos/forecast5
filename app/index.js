require("./styles.scss")

import React from 'react';
import { render } from 'react-dom';
import App from './modules/core/App.js';

render(
  <App />,
  document.getElementById('app')
);
