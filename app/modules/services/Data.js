require('whatwg-fetch') // fetch polyfill
import moment from 'moment';

import Config from './Config'
const { FORECAST_KEY, FORECAST_CITY, FORECAST_URL} = Config

export default {
  getUrl() {
    return `${FORECAST_URL}city?id=${FORECAST_CITY}&APPID=${FORECAST_KEY}&units=metric`
  },
  getForecast() {
    return fetch(this.getUrl())
      .then( (response) => {
        return response.json()
      }).then(this.groupByDay)
  },
  groupByDay(data) {
    let byDay = []

    return new Promise(function(resolve, reject) {
      data.list.map((entry)=>{
        let day = moment(
          new Date(entry.dt*1000)).startOf('day').format('DD/MM/YYYY');
        let lastElement = byDay.length - 1

        if(byDay.length && byDay[lastElement].day === day) {
          byDay[lastElement].entries.push(entry)
        } else {
          byDay.push({
            day: day,
            entries: [ entry ]
          })
        }
      })
      resolve(byDay)

    })
  },
}
