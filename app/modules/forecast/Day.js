import React from 'react'
import Entry from './Entry'

export default function ({day}) {
  var entries = day.entries.map((entry, idx) => {
    return (<Entry key={idx} entry={entry} />)
  })

  return (
    <div className='day'>
      <div className="day-title">
        {day.day}
      </div>
      <div className='day-entries'>
        {entries}
      </div>

    </div>
  )
}
