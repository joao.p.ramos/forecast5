import React from 'react'
import moment from 'moment'

export default function ({entry}) {
  return (
    <div className='entry'>
      <div className='entry-hour'>
        {moment(entry.dt*1000).hours()}h
      </div>
      <div className='entry-data'>
        <div> {entry.weather[0].main}  - {entry.weather[0].description}  </div>
        <div> Pressure: {entry.main.pressure} </div>
        <div> Humidy: {entry.main.humidity} </div>
        <div> Low: {entry.main.temp_min} </div>
        <div> High: {entry.main.temp_max} </div>
      </div>
    </div>
  )
}
