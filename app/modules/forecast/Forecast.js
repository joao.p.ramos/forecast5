require('./forecast.scss');

import React from 'react'
import Day from './Day'

export default function ({data}) {
  if(!data) {
    return null
  }

  var days =  data.map((entry, idx) => {
    return (<Day key={idx} day={entry} />)
  })

  return (
    <div className='forecast'>
      <div className='city'>
        {days}
      </div>
    </div>
  )
}
