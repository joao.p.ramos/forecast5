import React from 'react';

export default function () {
    return (
      <div className='footer'>
        <div>
          Source code:
          <a href='http://'>/</a>
        </div>
        <div>
          Demo:
          <a href='http://'>/</a>
        </div>
      </div>
    )
}
