import React from 'react';

export default function () {
  return (
    <div className='header'>
      <div> 5 days Forecast</div>
      <div>
        Powered by:
        <a href='http://openweathermap.org'>openweathermap.org/</a>
      </div>
    </div>
  )
}
