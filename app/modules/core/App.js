require('./main.scss')

import React from 'react';
import Data from '../services/data';
import Loader from './Loader'
import Header from './Header'
import Footer from './Footer'
import Forecast from '../forecast/Forecast'

export default React.createClass({
  getInitialState() {
    return { data: null, loading: true}
  },
  componentWillMount() {
    Data.getForecast().then( (data) => {
      this.setState({data: data, loading: false});
    })

  },
  render() {
    console.log(this.state)
    return (
      <div className="app">
        <Header />
        <Loader loading={this.state.loading} />
        <Forecast data={this.state.data} />
        <Footer />
      </div>
    )
  }
})
