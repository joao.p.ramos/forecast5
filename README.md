# Forecast 5

## Getting Started

Install the default dependencies as follows

```
$ npm install
```

## Develop

To run the development environment :

```
$ npm start         # starts webpack and serves on localhost:9000
```


## Test

Test the application

```
$ npm run test
```

There aren't any test yet.

## Build

To build:

```
$ npm run build         #  webpack builds to /build directory
```
