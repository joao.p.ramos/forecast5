let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let autoprefixer = require('autoprefixer');
let path = require('path');
let hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

let isProd = (process.env.NODE_ENV === 'production');

let config = {
    devServer: { inline:true},
    resolve: {
        modulesDirectories: ["node_modules", "bower_components", 'app/styles'],
        root: path.resolve('./')
    },
    entry: {
        scripts: isProd ? './app/index.js': ['./app/index.js', hotMiddlewareScript],
        bundle: './app/bundle.js'
    },
    output: {
        path: __dirname + '/build',
        publicPath: '/',
        filename: '[name].js'
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css!sass!postcss-loader')
            },
            { test: /\.html$/, loader: "html" },
            { test: /\.frag$/, loader: "html" },
            { test: /\.vert$/, loader: "html" },
            { test: /\.jpg$/, loader: "file-loader" },
            { test: /\.png$/, loader: "url-loader?mimetype=image/png" },
            {
              test: /\.js$/,
              exclude:  /(node_modules|bower_components|libs)/,
              loader: 'babel',
              query: {
                presets: ['es2015', 'react']
              }
            }
        ]
    },
    plugins: [
        // hot reload
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        // extract css to diferent file
        new ExtractTextPlugin('style.css', {
            allChunks: true
        }),
        // process html template
        new HtmlWebpackPlugin({
            title: JSON.stringify(require("./package.json").title),
            template: './app/html.tmpl',
            scripts: 'scripts.js',
            bundle: 'bundle.js',
            inject: false
        })
    ],
    sassLoader: {
        includePaths: [path.resolve(__dirname, "./app/styles")]
    },
    postcss: function() {
        return [ autoprefixer];
    }
}

module.exports = config;
